#!/usr/bin/env python3

import requests
from html.parser import HTMLParser
from bs4 import BeautifulSoup 
import os
import re
import cv2
import numpy as np
import argparse

def url_to_image(url):
    resp = requests.get(url)
    image = np.asarray(bytearray(resp.content), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)
    file_name = url.rsplit('/', 1)[-1]
    name = file_name.rsplit('.',1)[0]
    return {"img": image,"name": file_name,"name": name, "url": url}

def resize (img, max_h = 300, max_w = -1):
    f2 = max_h / img.shape[0]
    f1 = max_w / img.shape[1] if max_w > 0 else  img.shape[1]
    f = min(f1, f2)  # resizing factor
    dim = (int(img.shape[1] * f), int(img.shape[0] * f))
    return cv2.resize(img, dim)

def get_img_urls(url):
    res = requests.get(url)
    html = res.content
    soup = BeautifulSoup(html, 'html.parser') 
    
    img_tags = soup.find_all('img')
    return list(map(lambda n: n["src"], img_tags))

def setup_arg_parser():
    parser=argparse.ArgumentParser()
    parser.add_argument("--url", help="The target page to get images from", required=True)
    parser.add_argument("--maxh", help="Max height of output images", type = int, nargs='?', const = 1, default = 300)
    parser.add_argument("--output", help="The output directory, if left usued will use location from URL", nargs='?')
    parser.add_argument("--quality", help="Quality of output images (0-100)", type = int, nargs='?', const = 1, default = 90)
    return parser

def main():
    arg_parser = setup_arg_parser()
    args = arg_parser.parse_args()

    img_urls = get_img_urls(args.url)
    
    imgs = list(map(lambda n: url_to_image(n), img_urls))      
    
    if (args.output != None):
        output_dir = args.output
    else:
        regex = re.compile(r"https?://(www\.)?")
        output_dir = 'output/' + regex.sub('', args.url).strip().strip('/').rsplit('.', 1)[0] + '/'
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

    if os.path.exists(output_dir):
        user_input = input(f"Output path '{output_dir}' already exists..\ncontinue? (y/n)")
        if user_input != "y":
            print("Aborting")
            exit(1)

    for img in imgs:
        if not hasattr(img["img"], '__len__'):
            print("Skipping " + img["name"] + ", invalid image data")
            continue
        
        #TODO: check if already exists
        output_path = output_dir + "thumb_" + img["name"] + ".jpeg"
        thumb = resize(img["img"], args.maxh)
        cv2.imwrite(output_path, thumb,  [int(cv2.IMWRITE_JPEG_QUALITY), args.quality])

if __name__ == "__main__":
    main()